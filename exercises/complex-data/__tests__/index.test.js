const faker = require('faker');

// custom matcher to verify that string is a number
// highly probable that it can be done with stringMatching,
// but I consider it as a good exercise:)
expect.extend({
  toBeFloatNumericString(received) {
    const pass = !Number.isNaN(parseFloat(received));
    if (pass) {
      return {
        message: () => `expected string ${received} is a number`,
        pass: true,
      };
    }
    return {
      message: () => `expected string ${received} is not a number`,
      pass: false,
    };
  },
});

// BEGIN
describe('test createTransaction', () => {
  test('should have all attributes', () => {
    const transaction = faker.helpers.createTransaction();

    expect(transaction).toEqual(expect.objectContaining({
      amount: expect.toBeFloatNumericString(),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.stringMatching(/\d{8}/),
    }));
  });

  test('should generate different objects', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();
    expect(transaction1).not.toBe(transaction2);
  });
});
// END
