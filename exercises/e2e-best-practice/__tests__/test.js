// @ts-check

// BEGIN

describe('todo app', () => {
  beforeEach(async () => {
    await page.goto('http://0.0.0.0:8080');
  });

  test('page loads', async () => {
    const addButtonText = await page.$eval("input[data-testid='add-task-button']",
      (el) => el.getAttribute('value'));
    expect(addButtonText).toEqual('Add');
  });

  test('add tasks', async () => {
    const task1Text = 'new task text';
    const task2Text = 'another task 2 text';
    await expect(page).toFillForm('form', { text: task1Text });
    await page.click("input[data-testid='add-task-button']");
    await expect(page).toMatch(task1Text);

    await expect(page).toFillForm('form', { text: task2Text });
    await page.click("input[data-testid='add-task-button']");
    await expect(page).toMatch(task2Text);
  });

  test('remove tasks', async () => {
    const task1Text = 'task text to test remove';
    const task2Text = 'another task to test remove';

    await expect(page).toFillForm('form', { text: task1Text });
    await page.click("input[data-testid='add-task-button']");
    await expect(page).toFillForm('form', { text: task2Text });
    await page.click("input[data-testid='add-task-button']");

    await page.click('li.list-group-item > button');
    await page.waitForSelector('li.list-group-item:nth-child(2)', { hidden: true });
    await expect(page).not.toMatch(task1Text);
    await expect(page).toMatch(task2Text);

    await page.click('li.list-group-item > button');
    await page.waitForSelector('li.list-group-item', { hidden: true });
    await expect(page).not.toMatch(task1Text);
    await expect(page).not.toMatch(task2Text);
  });
});
// END
