import { rest, setupWorker } from 'msw';
import getNextId from '../lib/getNextId';

const handlers = [
  rest.get('/tasks', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([]),
    );
  }),
  rest.post('/tasks', (req, res, ctx) => {
    const { text } = req.body.task;
    const response = {
      text,
      id: getNextId(),
      state: 'active',
    };
    return res(
      ctx.status(200),
      ctx.json(response),
    );
  }),
  rest.delete('/tasks/:id', (req, res, ctx) => res(
    ctx.status(204),
  )),
];

const worker = setupWorker(...handlers);

export default worker;
