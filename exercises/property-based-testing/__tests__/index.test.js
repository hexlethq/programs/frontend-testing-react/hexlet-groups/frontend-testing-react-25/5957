const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN

describe('test default sort', () => {
  test('should preserve the length', () => {
    fc.assert(
      fc.property(
        fc.array(fc.double()), (data) => {
          const { length } = data;
          const sortedData = sort(data);
          const sortedLength = sortedData.length;
          expect(sortedLength).toEqual(length);
        },
      ),
    );
  });

  test('should sort array', () => {
    fc.assert(
      fc.property(
        fc.array(fc.double()), (data) => {
          const sortedData = sort(data);
          expect(sortedData).toBeSorted({ descending: false });
        },
      ),
    );
  });

  test('should be idempotent', () => {
    fc.assert(
      fc.property(
        fc.array(fc.double()), (data) => {
          const sortedData = sort(data);
          const doubleSortedData = sort(sortedData);
          expect(sortedData).toEqual(doubleSortedData);
        },
      ),
    );
  });

  test('should contain the same elements, double', () => {
    fc.assert(
      fc.property(
        fc.array(fc.double()), (data) => {
          const sortedData = sort(data);
          data.forEach((element) => expect(sortedData).toContain(element));
        },
      ),
    );
  });

  test('should contain the same elements, integer', () => {
    const count = (tab, element) => tab.filter((v) => v === element).length;

    fc.assert(
      fc.property(
        fc.array(fc.integer()), (data) => {
          const sortedData = sort(data);
          data.forEach(
            (element) => expect(count(sortedData, element)).toEqual(count(data, element)),
          );
        },
      ),
    );
  });
});

// END
