// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');

const run = require('../src/application');

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
test("initial state", () => {
  const listsList = document.querySelector("div[data-container='lists']")
      .querySelectorAll('li');
  expect(listsList).toHaveLength(1);
  expect(listsList[0].innerHTML).toContain("General");

  const tasksList = document.querySelector("div[data-container='tasks']")
      .querySelectorAll('li');
  expect(tasksList).toHaveLength(0);
});

// Задача добавляется в список по умолчанию (General).
// После добавления задачи поле ввода сбрасывается и добавленная задача
// отображатеся на странице в соответствующем блоке.
test("create task", async () => {
  const taskInput = document.querySelector("input[data-testid='add-task-input']");
  userEvent.type(taskInput, 'blah');
  // taskInput.value = 'blah';
  // await taskInput.click();

  const tasksList = document.querySelector("div[data-container='tasks']");
  // expect(tasksList).not.toBeEmptyDOMElement()
})





// END
