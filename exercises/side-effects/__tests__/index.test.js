const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index');

const getPathToFixture = (filename) => path.resolve(__dirname, '../__fixtures__/', filename);

const prepareFixtures = () => {
  const common = { version: '1.3.2' };
  fs.writeFileSync(getPathToFixture('package.json'), JSON.stringify(common));

  fs.writeFileSync(getPathToFixture('packageInvalidJson.json'), 'Invalid Json');

  const noVersion = { name: 'superpackage' };
  fs.writeFileSync(getPathToFixture('packageWithoutVersion.json'),
    JSON.stringify(noVersion));

  const invalidVersion = { version: '1.X.4' };
  fs.writeFileSync(getPathToFixture('packageWithInvalidVersion.json'),
    JSON.stringify(invalidVersion));
};

const readVersion = (filename) => {
  const content = JSON.parse(fs.readFileSync(filename, 'utf-8'));
  return content.version;
};

const negativeCasesData = [
  {
    description: 'file is unreadable',
    filename: 'nonExistingFile',
    expectedError: /Unreadable file/,
  }, {
    description: 'file not in JSON format',
    filename: 'packageInvalidJson.json',
    expectedError: /Invalid JSON/,
  }, {
    description: 'package.json doesnt have a version',
    filename: 'packageWithoutVersion.json',
    expectedError: /doesn't contain version/,
  }, {
    description: 'package.json contains version not in semVer format',
    filename: 'packageWithInvalidVersion.json',
    expectedError: /Wrong semver format/,
  },
];

describe('test function upVersion', () => {
  beforeEach(() => prepareFixtures());

  test('should throw an error if version part is incorrect', () => {
    expect(() => upVersion('package.json', 'invalidPart'))
      .toThrowError(/Unknown version part/);
  });

  test.each(negativeCasesData)('should throw an error if $description',
    ({ filename, expectedError }) => {
      const pathToPackageJson = getPathToFixture(filename);
      expect(() => upVersion(pathToPackageJson)).toThrowError(expectedError);
    });

  test.each([['patch', '1.3.3'], ['minor', '1.4.0'], ['major', '2.0.0']])('should increase version correctly, part: %s',
    (versionPart, expectedResult) => {
      const packageJsonPath = getPathToFixture('package.json');
      upVersion(packageJsonPath, versionPart);
      const version = readVersion(packageJsonPath);
      expect(version).toEqual(expectedResult);
    });

  test('should increase version correctly with default patch version part', () => {
    const packageJsonPath = getPathToFixture('package.json');
    upVersion(packageJsonPath);
    const version = readVersion(packageJsonPath);
    expect(version).toEqual('1.3.3');
  });
});
