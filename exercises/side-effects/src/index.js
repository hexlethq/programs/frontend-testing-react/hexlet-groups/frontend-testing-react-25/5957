const fs = require('fs');

const versionPartPositionMap = {
  major: 0,
  minor: 1,
  patch: 2,
};

const semVerRegEx = /(\d+)\.(\d+)\.(\d+)/;

const parseVersion = (version) => {
  const match = semVerRegEx.exec(version);
  if (match === null || match.length !== 4) {
    throw new Error(`Wrong semver format: ${version}`);
  }
  return [match[1], match[2], match[3]];
};

const upVersion = (packageJsonPath, versionPart = 'patch') => {
  const versionPartPosition = versionPartPositionMap[versionPart];
  if (versionPartPosition === undefined) {
    throw new Error(`Unknown version part: ${versionPart}`);
  }

  let rawContent = null;
  try {
    rawContent = fs.readFileSync(packageJsonPath, 'utf-8');
  } catch (e) {
    throw new Error(`Unreadable file: ${packageJsonPath}`);
  }

  let content = null;

  try {
    content = JSON.parse(rawContent);
  } catch (e) {
    throw new Error(`Invalid JSON format: ${rawContent}`);
  }

  if (!('version' in content)) {
    throw new Error(`package.json doesn't contain version: ${content}`);
  }

  const version = parseVersion(content.version);
  version[versionPartPosition] = parseInt(version[versionPartPosition], 10) + 1;
  for (let i = versionPartPosition + 1; i < version.length; i += 1) {
    version[i] = 0;
  }
  const newVersion = version.join('.');

  const newContent = { ...content, version: newVersion };
  try {
    fs.writeFileSync(packageJsonPath, JSON.stringify(newContent), 'utf-8');
  } catch (e) {
    throw new Error(`Error while writing to file: ${packageJsonPath}`);
  }
};

module.exports = { upVersion };
