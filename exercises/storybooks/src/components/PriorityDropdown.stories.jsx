import React from 'react';

import PriorityDropdown from './PriorityDropdown';

// BEGIN
export default {
    component: PriorityDropdown,
    title: "PriorityDropdown"
};

const Template = args => <PriorityDropdown {...args}/>;

export const Default = Template.bind({});
Default.args = {
    variant: "warning"
};

export const BigSize = Template.bind({});
BigSize.args = {
    ...Default.args,
    size: 'lg'
};

export const SmallSize = Template.bind({});
SmallSize.args = {
    ...Default.args,
    size: 'sm'
};

export const DropRight= Template.bind({});
DropRight.args = {
    ...Default.args,
    dropDirection: "right"
};

// END
