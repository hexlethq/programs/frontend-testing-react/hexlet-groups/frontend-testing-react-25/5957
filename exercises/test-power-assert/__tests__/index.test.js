const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN

const array = [1, [2, [3, [4]], 5]];
const arrayWith1Flatten = [1, 2, [3, [4]], 5];
const arrayWith2Flattens = [1, 2, 3, [4], 5];
const arrayWith3Flattens = [1, 2, 3, 4, 5];

assert.deepEqual(flattenDepth(array), arrayWith1Flatten);
assert.deepEqual(flattenDepth(array, 1), arrayWith1Flatten);
assert.deepEqual(flattenDepth(array, 2), arrayWith2Flattens);
assert.deepEqual(flattenDepth(array, 3), arrayWith3Flattens);
assert.deepEqual(flattenDepth(array, 4), arrayWith3Flattens);
assert.deepEqual(flattenDepth(array, 100), arrayWith3Flattens);
assert.deepEqual(flattenDepth('pew'), ['p', 'e', 'w']);

assert.deepEqual(flattenDepth(array, -1), array);
assert.deepEqual(flattenDepth(array, 0.5), array);
assert.deepEqual(flattenDepth(array, 'abc'), array);
assert.deepEqual(flattenDepth([]), []);
assert.deepEqual(flattenDepth({ name: 'bob' }), []);
assert.deepEqual(flattenDepth([{ name: 'bob' }], 3), [{ name: 'bob' }]);

// END
