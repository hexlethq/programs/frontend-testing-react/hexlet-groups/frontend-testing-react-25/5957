describe('test assign function', () => {
  expect.hasAssertions();

  test("source hasn't been changed", () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    Object.assign(target, src);
    expect(src).toEqual({ k: 'v', b: 'b' });
  });

  test('target has been updated correctly', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const expectedResult = { k: 'v', a: 'a', b: 'b' };
    Object.assign(target, src);
    expect(target).toEqual(expectedResult);
  });

  test('target and returned result are the same object', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, src);
    expect(result).toBe(target);
  });

  test('result has been calculated correctly', () => {
    const src = { k: 'v', b: 'b' };
    const target = { k: 'v2', a: 'a' };
    const expectedResult = { k: 'v', a: 'a', b: 'b' };
    const result = Object.assign(target, src);
    expect(result).toEqual(expectedResult);
  });

  test.each([{}, null, undefined])("assign %s doesn't change target", (source) => {
    const target = { k: 'v2', a: 'a' };
    const result = Object.assign(target, source);
    expect(result).toEqual({ k: 'v2', a: 'a' });
  });

  test('fail to write to non-writable target', () => {
    const src = { k: 'v', b: 'b' };
    const freezedTarget = Object.freeze({ c: 'c' });
    expect(() => Object.assign(freezedTarget, src)).toThrow(TypeError);
    expect(freezedTarget).toEqual({ c: 'c' });
  });

  test('fail to write to non-writable property', () => {
    const target = Object.defineProperty({}, 'foo', {
      value: 1,
      writable: false,
    });
    expect(() => Object.assign(target, { foo: 10 })).toThrow(TypeError);
  });

  test('support of multiple sources', () => {
    const src1 = { a: 'a' };
    const src2 = { b: 'b' };
    const target = { a: 'ZZZ', c: 'c' };
    const expectedResult = { a: 'a', b: 'b', c: 'c' };
    expect(Object.assign(target, src1, src2)).toEqual(expectedResult);
  });

  test('order is respected for multiple sources', () => {
    const src1 = { a: 'a1' };
    const src2 = { a: 'a2' };
    const target = { a: 'a0' };
    const expectedResult = { a: 'a2' };
    expect(Object.assign(target, src1, src2)).toEqual(expectedResult);
  });
});
