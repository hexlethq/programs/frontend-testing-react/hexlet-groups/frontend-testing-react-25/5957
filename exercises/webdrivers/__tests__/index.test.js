const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-gpu'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  test('should load main page', async () => {
    await page.goto(appUrl);
    const title = await page.$eval('#title', (el) => el.innerText);
    expect(title).toEqual('Welcome to a Simple blog!');
    const createMessage = await page.$eval('.lead', (el) => el.innerText);
    expect(createMessage).toEqual('Create new articles quickly and easily.');

    const articlesLink = await page.$eval('a.nav-link', (el) => el.href);
    expect(articlesLink).toEqual(appArticlesUrl);
  });

  test('should navigate to articles', async () => {
    await page.goto(appArticlesUrl);
    const title = await page.$eval('h3', (el) => el.innerText);
    expect(title).toEqual('Articles');

    const tableBody = await page.$('table > tbody');
    const articles = await tableBody.$$('tr');
    expect(articles).toHaveLength(4);
  });

  test('should open form for creating articles', async () => {
    await page.goto(appArticlesUrl);
    await Promise.all([
      page.waitForNavigation(),
      page.click('h3 + a'),
    ]);
    expect(page.url()).toEqual(`${appArticlesUrl}/new`);

    const title = await page.$eval('h3', (el) => el.innerText);
    expect(title).toEqual('Create article');
    const form = await page.$('form');
    expect(form).not.toBeNull();
  });

  test('should create an article', async () => {
    const expectedName = 'random article name';
    const expectedContent = 'random article content';
    const expectedCategory = 'omnis quisquam quisquam';

    await page.goto(`${appArticlesUrl}/new`);
    await page.type('#name', expectedName);
    await page.select('#category', '3');
    await page.type('#content', expectedContent);
    await page.click(".btn[value='Create']");
    await page.waitForSelector('table');
    expect(page.url()).toEqual(appArticlesUrl);

    const bodyHandle = await page.$('html');
    const bodyContent = await bodyHandle.$eval('body', (el) => el.innerHTML);
    expect(bodyContent).toContain(expectedName);
    expect(bodyContent).toContain(expectedCategory);

    const articles = await page.$$('table > tbody > tr');
    expect(articles).toHaveLength(5);
  });

  test('should edit an article', async () => {
    await page.goto(appArticlesUrl);
    const expectedName = 'new article name';
    const foundBeforeEdit = await page.evaluate(() => window.find('new article name'));
    expect(foundBeforeEdit).toBeFalsy();
    await page.click("a[href='/articles/4/edit']");

    await page.waitForSelector('#name');
    expect(page.url()).toEqual(`${appArticlesUrl}/4/edit`);

    await page.evaluate(
      (selector) => {
        (document.querySelector(selector).value = '');
      },
      '#name',
    );
    await page.type('#name', expectedName);
    await page.click("input[value='Update']");
    await page.waitForSelector('table');

    const foundAfterEdit = await page.evaluate(() => window.find('new article name'));
    expect(foundAfterEdit).toBeTruthy();
  });
  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
