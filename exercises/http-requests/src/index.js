const axios = require('axios');

// BEGIN
// don't throw exception on 4xx and 5xx
axios.defaults.validateStatus = () => true;

const get = async (url) => {
  const response = await axios.get(url);
  if (response.status !== 200) {
    throw new Error(`Problems with url: ${url}. Status: ${response.status}`);
  }
  return response.data;
};

const post = async (url, body) => {
  const response = await axios.post(url, body);
  if (response.status !== 200) {
    throw new Error(`Problems with url: ${url}. Status: ${response.status}`);
  }
};
// END

module.exports = { get, post };
