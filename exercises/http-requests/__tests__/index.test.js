const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
const urlBase = 'http://example.com';
const urlPath = 'user/bob';
const url = `${urlBase}/${urlPath}`;

describe('test get and post functions', () => {
  beforeAll(() => {
    nock.disableNetConnect();
  });

  afterEach(() => {
    nock.cleanAll();
  });

  test('get, positive', async () => {
    const expectedResponse = {
      name: 'bob',
      age: 18,
    };
    nock(urlBase).get(`/${urlPath}`).reply(200, expectedResponse);

    const response = await get(url);

    expect(response).toEqual(expectedResponse);
  });

  test('get, negative', async () => {
    nock(urlBase).get(`/${urlPath}`).reply(500, { error: 'Url in unavailable' });
    await expect(() => get(url)).rejects.toThrowError(/Problems with url/);
  });

  test('post, positive', async () => {
    const body = {
      name: 'kent',
      age: 21,
    };
    nock(urlBase).post(`/${urlPath}`).reply(200, {});

    await post(url, body);

    expect(nock.isDone()).toBeTruthy();
  });

  test('post, negative', async () => {
    nock(urlBase).post(`/${urlPath}`).reply(500, { error: 'Url in unavailable' });
    await expect(() => post(url, {})).rejects.toThrowError(/Problems with url/);
  });
});
// END
